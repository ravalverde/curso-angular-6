import { AngularWishListPage } from './app.po';

describe('angular-wish-list App', function() {
  let page: AngularWishListPage;

  beforeEach(() => {
    page = new AngularWishListPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
