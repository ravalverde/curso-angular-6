import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { DestinoDetalleComponentComponent } from './destino-detalle-component/destino-detalle-component.component';
import { FormDetinoViajeComponent } from './form-detino-viaje/form-detino-viaje.component';

import { StoreModule as NgRxStoreModule, ActionReducerMap } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';


const routes: Routes = [
{ path: '', redirectTo: 'home', pathMatch: 'full' },
{ path: 'home', component: ListaDestinosComponent },
{ path: 'destino/:id', component: DestinoDetalleComponentComponent }
];

import {
  DestinosViajesState,
  intializeDestinosViajesState,
  reducerDestinosViajes,
  DestinosViajesEffects
} from './models/destinos-viajes-state-model';

// redux init
export interface AppState {
  destinos: DestinosViajesState;
}

const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes
};

const reducersInitialState = {
  destinos: intializeDestinosViajesState()
};

const environment = {
  production: false
};
// redux fin init

@NgModule({
  declarations: [
  AppComponent,
  DestinoViajeComponent,
  ListaDestinosComponent,
  DestinoDetalleComponentComponent,
  FormDetinoViajeComponent

  ],
  imports: [
  BrowserModule,
  RouterModule.forRoot(routes),
  NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState }),
  EffectsModule.forRoot([DestinosViajesEffects]),
  FormsModule,
  ReactiveFormsModule,
  HttpClientModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
