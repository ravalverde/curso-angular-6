import { Component, OnInit, Input,Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destinoViaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { VoteUpAction, VoteDownAction } from '../models/destinos-viajes-state-model';

@Component({
	selector: 'app-destino-viaje',
	templateUrl: './destino-viaje.component.html',
	styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
	@Input() destino: DestinoViaje; //recibiendo el objeto como parametro para utilizarlo
	//@HostBinding('attr.class') cssClass = 'col-md-4'; HostBinding no funciona agregré la clase directo al component
	@Output() clicked: EventEmitter<DestinoViaje>;
	@Input() posicion: number;
	@Input('idx') posicion1: number;
	constructor(private store: Store<AppState>) { 
		this.clicked = new EventEmitter;
	}
	ir (){
		this.clicked.emit(this.destino)
		return false;
	}

    voteUp(){
        this.store.dispatch(new VoteUpAction(this.destino));
        return false;
    }
    voteDown(){
        this.store.dispatch(new VoteDownAction(this.destino));
        return false;
    }

	ngOnInit() {
	}

}
