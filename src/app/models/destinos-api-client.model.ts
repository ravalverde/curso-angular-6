import {Injectable, Inject, forwardRef} from '@angular/core';
import { DestinoViaje } from './../models/destinoViaje.model';
import {Subject, BehaviorSubject,Observable} from 'rxjs';

@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[];
  current: Subject <DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
  private destinos$ = new Subject<DestinoViaje>();

  constructor (){

    this.destinos = [];
  } 

  agregarDestino (d:DestinoViaje){
    this.destinos.push(d);
  }
  getAll():DestinoViaje[]{
    return this.destinos;

  }
  elegir (d: DestinoViaje){
    this.destinos.forEach(x=> x.setSelected(false))
    d.setSelected(true);
    this.current.next(d);
  }

  subscribeOnChange (fn){
    this.current.subscribe(fn);

  }
}